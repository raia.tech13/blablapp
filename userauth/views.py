from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse
from .forms import LoginForm, ProfileEditForm, RegisterForm
#from django.contrib.auth.forms import AuthenticationForm
from .models import Profile, User
from django.contrib.auth.decorators import login_required
from django.utils import timezone

 
@login_required
def user_profil(request):
    user_profil = Profile.objects.filter(user=request.user)
    print('userp', user_profil)
    user = User.objects.filter(username=request.user)
    print('useru', user)
    user_mail = [ item.email for item in user]
    print('mail', user_mail)
    if request.method == 'POST':
        form = ProfileEditForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
    form = ProfileEditForm()
    mail = RegisterForm()
    context = {'section': 'profil', 'avatar_form': form,'user': user_profil, 'mail': user_mail, 'form_mail': mail}
    return render(request, 'userauth/profil.html', context)

 
def register(request):
    if (request.method == "POST"):
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(
                username=request.POST['username'], 
                password=request.POST['password1']
            )
            login(request, user)
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'userauth/register.html', { 'form': form })




def user_login(request):
    if request.method == 'POST':
        user = authenticate(request,
                                username=request.POST['username'],
                                password=request.POST['password'])
        print('user', user)
        if user is not None:
            login(request, user)
            return redirect('index')
    form = LoginForm()
    return render(request, 'userauth/login.html', {'form': form})



def user_logout(request):
    logout(request)
    return redirect('index')

def rgpd(request):
    return render(request, 'userauth/terms_conditions.html')



