from django import forms
from .models import ChatRoom, Chat
from userauth.models import Profile


class ContactsForm(forms.ModelForm):
    class Meta:
        model = ChatRoom
        fields = ['room_name', 'members']
        widgets = {
            'members': forms.CheckboxSelectMultiple()
        }

class ChatForm(forms.ModelForm):
    class Meta:
        model = Chat
        fields = ['room_name', 'author', 'message']
        widgets = {
            'author': forms.HiddenInput(),
            'room_name': forms.HiddenInput(),
            'message': forms.Textarea(attrs={'rows':1, 'placeholder':'message'}) 
        }
