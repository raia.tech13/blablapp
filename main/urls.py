
from django.urls import path, include
from .           import views

urlpatterns = [
    path('', views.index, name='index'),
    #path('wall', views.wall, name='wall'),
    path('contacts', views.contacts, name='contacts'),
    #path('<int:question_id>/vote/', views.vote, name='vote'),
    #path('post', views.post, name='post'),
    path('<int:id>', views.chatting, name='chatting'),
    #path('talk', views.TalkMain.as_view(), name='talk'),
    #path('messages', views.Talkmessages, name='messages'),
]
