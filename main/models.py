from django.db import models

from django.conf import settings
from userauth.models import Profile
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone




class ChatRoom(models.Model):
    room_name = models.CharField(max_length=200, default='')
    members = models.ManyToManyField(Profile)
    def __unicode__(self):
        return self.members

class Chat(models.Model):
    room_name = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    author =  models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, default='' )
    message = models.TextField(null=True, default='')

    def __unicode__(self):
        return self.message








