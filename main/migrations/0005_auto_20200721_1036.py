# Generated by Django 3.0.6 on 2020-07-21 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userauth', '0007_auto_20200714_1119'),
        ('main', '0004_auto_20200721_0921'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chatroom',
            name='contact',
        ),
        migrations.AddField(
            model_name='chatroom',
            name='contact',
            field=models.ManyToManyField(to='userauth.Profile'),
        ),
    ]
