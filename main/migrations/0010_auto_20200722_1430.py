# Generated by Django 3.0.6 on 2020-07-22 14:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('userauth', '0007_auto_20200714_1119'),
        ('main', '0009_auto_20200722_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='author',
            field=models.ForeignKey(default='', null=True, on_delete=django.db.models.deletion.CASCADE, to='userauth.Profile'),
        ),
    ]
