from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Chat, Profile, ChatRoom
from .forms import ContactsForm, ChatForm



def index(request):
    if request.user.is_authenticated:
        chatrooms = ChatRoom.objects.filter(members=request.user.profile)
    else:
        chatrooms = " "
    chats = Chat.objects.all()
    context = {'wall': 'active', 'chat': chats, 'chatrooms': chatrooms}
    return render(request, 'main/index.html', context)


def contacts(request):
    form = ContactsForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            c = form.save()
            c.members.add(request.user.profile)
            return redirect('index')
        else:
            return redirect('index')
    context = {
        'form': form,
        }   
    return render(request, 'main/contacts.html', context)


def chatting(request, id):
    if request.user.is_authenticated:
        chatrooms = ChatRoom.objects.filter(members=request.user.profile)
    else:
        chatrooms = None
    chats = Chat.objects.filter(room_name=id)
    room_choice = ChatRoom.objects.filter(id=id)
    url = room_choice[0].id
    form = ChatForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            form = ChatForm()     
    context = {
        'wall': 'active',
        'chats': chats,
        'chatrooms': chatrooms,
        'room_choice': room_choice,
        'chatform': form, 
        'url': url
        }
    return render(request, 'main/index.html', context)



